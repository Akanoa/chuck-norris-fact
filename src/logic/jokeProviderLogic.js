import {inject, onMounted} from "vue";
import {StoreSymbol} from "../store";

export default function buildJokeProvider() {

    let store = inject(StoreSymbol, null);
    if (!store) return;

    onMounted(() => {
        getJoke();
    });

    function getJoke() {
        fetch("http://api.icndb.com/jokes/random")
            .then(response => response.json())
            .then(result => {
                store.mutations.updateJoke(result.value.joke)
            })
    }

    return {
        getJoke,
        addJoke: store.mutations.addJokeDisplay
    }
}