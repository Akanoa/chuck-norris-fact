import {computed, inject, ref, unref} from "vue";
import {StoreSymbol} from "../store";

export default function () {
    const store = inject(StoreSymbol, null)
    if (!store) return;
    
    let replacementRef = ref("Chuck Norris");
    let replacement = computed({
        get: () => replacementRef.value,
        set: val => replacementRef.value = unref(val)
    });

    let text = computed(() => {
        return store.state.joke.replace("Chuck Norris", replacementRef.value)
    })
    return {text, replacement}
}