import {reactive} from "vue";

let state = {
    joke : "",
    jokeDisplays: []
}

const mutations = {
    updateJoke(joke) {
        store.state.joke = joke
    },
    removeJokeDisplay(index) {
        console.log("remove index "+index)
        store.state.jokeDisplays.splice(index, 1)
    },
    addJokeDisplay() {
        store.state.jokeDisplays.push(Symbol())
    }
}

export const StoreSymbol = Symbol();

export const store = reactive({
    state,
    mutations
})